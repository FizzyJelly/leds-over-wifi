// PWM configuration
const uint8_t PWM_RESOLUTION = 255;
volatile uint8_t pwm_level = 0;

// button logic vars
volatile int32_t brightness = 0;
const uint8_t brightness_levels = 12;
volatile double exponent = 1;


// debouncing
volatile uint32_t last_pressed = 0;
const uint8_t offset = 50; // miliseconds !!! affected by TCB prescaling

void down_isr(void);
void up_isr(void);

//Pin def
const short button_down_pin = 2;
const short button_up_pin = 3;

float calculate_exponent() {
  extern volatile double exponent;
  exponent = log((double) PWM_RESOLUTION) / log((double )brightness_levels - 1);
}

inline uint8_t calculate_pwm(uint8_t brightness) {
  extern volatile double exponent;
  return (uint8_t)(pow((double) brightness, exponent));
}

inline void set_pwm(uint8_t pwm_level) {
  TCB0.CCMP = ((pwm_level << 8) | 0x00FF);
}


void setup()
{
  calculate_exponent();

  //Switch TCB0 output pin to alternative Digital 6
  PORTMUX.TCBROUTEA |= 1;

  // enable writing to protected register
  CPU_CCP = CCP_IOREG_gc;

  // Setup clock divider
  CLKCTRL.MCLKCTRLB = CLKCTRL_PDIV_4X_gc | CLKCTRL_PEN_bm;

  //select 20MHz oscilator
  CLKCTRL.MCLKCTRLA = CLKCTRL_CLKSEL_OSC20M_gc;

  //wait for the change to finish
  while (CLKCTRL.MCLKSTATUS & CLKCTRL_SOSC_bm)
  {
    ;
  }

  sei();
  start_timer();

  pinMode(button_down_pin, INPUT_PULLUP);
  pinMode(button_up_pin, INPUT_PULLUP);
  pinMode(6, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(button_down_pin), down_isr, FALLING);
  attachInterrupt(digitalPinToInterrupt(button_up_pin), up_isr, FALLING);
}

void start_timer()
{
  // initial pwm setting
  TCB0.CCMP = 0x00FF;

  //enable clock in 8bit pwm mode
  TCB0.CTRLA |= TCB_ENABLE_bm;
  TCB0.CTRLB |= TCB_CCMPEN_bm | TCB_CNTMODE_PWM8_gc;

}

void down_isr() {
  extern volatile int32_t brightness;
  extern volatile uint8_t pwm_level;
  extern volatile uint32_t last_pressed;
  
  if(millis() - last_pressed > offset){
    brightness = max(0, brightness - 1);
    set_pwm(calculate_pwm(brightness));
    last_pressed=millis();
  }

}

void up_isr() {
  extern volatile int32_t brightness;
  extern volatile uint8_t pwm_level;
  extern volatile uint32_t last_pressed;
  
  if(millis() - last_pressed > offset){
    brightness = min(brightness_levels - 1, brightness + 1);
    set_pwm(calculate_pwm(brightness));
    last_pressed=millis();
  }
}



void loop(void)
{
}
