package com.example.led_controller

import android.graphics.Color
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.example.led_controller.model.BrightnessController
import kotlinx.android.synthetic.main.activity_led_menu.*

class LedActivity : AppCompatActivity() {

    private val presenter: BrightnessPresenter =
        BrightnessPresenter(
            this,
            BrightnessController()
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_led_menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        class ProgressListener : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                presenter.changeBrightness(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        }

        brightnessBar.setOnSeekBarChangeListener(ProgressListener())
    }

    fun setPreviewColor(colour: String) {
        brightnessPreview.setBackgroundColor(Color.parseColor(colour))
    }

    fun setSeekBarProgress(progress: Int) {
        brightnessBar.progress = progress
    }
}
