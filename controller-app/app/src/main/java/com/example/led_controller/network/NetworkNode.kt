package com.example.led_controller.network

interface NetworkNode {
    fun send(message: String)
    fun listen()
}