package com.example.led_controller.network

import android.util.Log

class LedControllerProtocol {

    fun getChangeBrightnessMessage(level: Int): String {
        return "S%$level"
    }

    fun parseMessage(message: String): ControllerMessage? {
        if (message.isNotEmpty()) {
            val code = message[0]
            if (message.length >= 3 && message[1] == '%') {
                val data = message.substring(2)
                return when (code) {
                    'H' -> {
                        ControllerMessage(MessageType.HELLO, data)
                    }
                    'S' -> {
                        ControllerMessage(MessageType.STATUS, data)
                    }
                    else -> {
                        null
                    }
                }
            } else {
                Log.d("MSG", "Message has invalid format: $message")
            }
        }

        return null
    }
}

enum class MessageType {
    HELLO,
    STATUS
}

class ControllerMessage(val type: MessageType, val data: String)