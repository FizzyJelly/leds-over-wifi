package com.example.led_controller.network

interface MessageHandler {
    fun onMessageReceived(message: String)
}