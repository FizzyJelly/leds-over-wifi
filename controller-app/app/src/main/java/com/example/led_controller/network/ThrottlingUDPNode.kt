package com.example.led_controller.network

import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.locks.ReentrantLock

class ThrottlingUDPNode(
    private val delayInterval: Long,
    address: String,
    port: Int,
    messageHandler: MessageHandler
) : UDPNode(address, port, messageHandler) {

    private val messageQueue = LinkedList<String>()
    private val lock = ReentrantLock()



    override fun send(message: String) = runBlocking {
        lock.lock()
        if (messageQueue.size == 0) {
            messageQueue.add(message)
            launch {
                launchMessage()
            }
        } else {
            messageQueue.add(message)
        }
        lock.unlock()
    }

    private suspend fun launchMessage() {
        withContext(Dispatchers.IO) {
            runBlocking {
                delay(delayInterval)
            }
            lock.lock()
            val message: String = messageQueue.pop()
            super.send(message)
            if (messageQueue.isNotEmpty()) {
                launchMessage()
            }
            lock.unlock()
        }
    }
}