package com.example.led_controller.model

import com.example.led_controller.network.*
import java.lang.NumberFormatException
import java.net.DatagramSocket
import java.net.InetAddress

class BrightnessController {

    var brightnessChangeListener: BrightnessChangeListener? = null

    class MessageHandler(
        private val communicator: BrightnessController,
        private val protocol: LedControllerProtocol
    ) :
        com.example.led_controller.network.MessageHandler {
        override fun onMessageReceived(message: String) {
            val parsed = protocol.parseMessage(message)
            if (parsed != null)
                communicator.onMessageReceived(parsed)
        }
    }

    interface BrightnessChangeListener {
        fun onBrightnessChanged(level: Int)
    }

    private val protocol: LedControllerProtocol =
        LedControllerProtocol()

    private var networkNode: NetworkNode =

        ThrottlingUDPNode(
            10,
            "255.255.255.255",
            8888,
            MessageHandler(
                this,
                protocol
            )
        )

    private fun onMessageReceived(message: ControllerMessage) {

        when (message.type) {
            MessageType.HELLO -> {
                TODO("Not implemented")
            }
            MessageType.STATUS -> {
                try {
                    val level = message.data.toInt(10)
                    if (level < 0)
                        throw NumberFormatException()
                    if (level > 255)
                        throw NumberFormatException()

                    brightnessChangeListener?.onBrightnessChanged(level)
                } catch (ex: NumberFormatException) {
                    return
                }
            }
        }
    }

    fun setBrightness(level: Int) {
        networkNode.send(protocol.getChangeBrightnessMessage(level));
    }

}