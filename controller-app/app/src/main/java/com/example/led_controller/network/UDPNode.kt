package com.example.led_controller.network


import android.util.Log
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

open class UDPNode(
    private val address: String,
    private val port: Int,
    private val messageHandler: MessageHandler
) :
    NetworkNode {

    private val socket: DatagramSocket = DatagramSocket()

    init {
        socket.broadcast = true;
    }

    override fun send(message: String) {
        Log.d(null, "Sending message $message")
        try {
            socket.send(
                DatagramPacket(
                    message.toByteArray(),
                    message.length,
                    InetAddress.getByName(address),
                    port
                )
            )

        } catch (ex: IOException) {
            Log.d("ERR", "Failed to send message. Cause: ${ex.message}")
        }

    }

    override fun listen() {
//        launch {
//            asyncListen()
//        }
    }

    private suspend fun asyncListen() {

    }
}