package com.example.led_controller

import com.example.led_controller.model.BrightnessController
import com.example.led_controller.model.BrightnessController.BrightnessChangeListener as BrightnessChangeListener


class BrightnessPresenter(
    private var brightnessView: LedActivity?,
    private var controller: BrightnessController
) :
    BrightnessChangeListener {

    init {
        controller.brightnessChangeListener = this
    }

    fun changeBrightness(level: Int) {
        controller.setBrightness(level)
        onBrightnessChanged(level)
    }

    override fun onBrightnessChanged(level: Int) {
        brightnessView?.setPreviewColor(getGrayScaleColorString(level))
        brightnessView?.setSeekBarProgress(level)
    }

    fun onDestroy() {
        brightnessView = null
    }

    private fun getGrayScaleColorString(level: Int): String {
        var hex = "%x".format(level)
        if (hex.length == 1)
            hex = "0$hex"
        return "#${hex.repeat(3)}"
    }
}